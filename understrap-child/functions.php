<?php
add_action( 'init', 'register_post_types' );

function register_post_types(){

	register_post_type( 'realty', [
		'label'  => null,
		'labels' => [
			'name'               => 'Недвижимость',
			'singular_name'      => 'объект недвижимости',
			'add_new'            => 'Добавить объект',
			'add_new_item'       => 'Добавление объекта',
			'edit_item'          => 'Редактирование объекта',
			'new_item'           => 'Новый объект',
			'view_item'          => 'Смотреть объект',
			'search_items'       => 'Искать объект',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Недвижимость',
		],
		'description'            => '',
		'public'                 => true,		
		'show_in_menu'           => null,		
		'show_in_rest'        => null, 
		'rest_base'           => null, 
		'menu_position'       => null,
		'menu_icon'           => null,
		
		'hierarchical'        => false,
		'supports'            => [ 'title', 'editor' ],
		'taxonomies'          => ['Тип недвижимости'],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	] );
}

add_action( 'init', 'Create_tax_realty', 0 );

function Create_Tax_realty () 
{
    $args = array(
        'label' => _x( 'тип недвижимости', 'taxonomy general name' ),
        'labels' => array(
        'name' => _x( 'тип недвижимости', 'taxonomy general name' ),
        'singular_name' => _x( 'НЕдвижимость', 'taxonomy singular name' ),
        'menu_name' => __( 'Недвижимость' ),
        'all_items' => __( 'Вся недвижимость' ),
        'edit_item' => __( 'Изменить недвиждимость' ),
        'view_item' => __( 'Просмотреть недвижимость' ),
        'update_item' => __( 'Обновить недвижимость' ),
        'add_new_item' => __( 'Добавить новую недвижимость' ),
        'new_item_name' => __( 'Название' ),
        'parent_item' => __( 'Родительская' ),
        'parent_item_colon' => __( 'Родительская:' ),
        'search_items' => __( 'Поиск недвижимости' ),
        'popular_items' => null,
        'separate_items_with_commas' => null,
        'add_or_remove_items' => null,
        'choose_from_most_used' => null,
        'not_found' => __( 'Недвижимость не найдена.' ),
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_in_rest' => true,
        'meta_box_cb' => null,
        'show_admin_column' => false,
        'description' => '',
        'hierarchical' => true,
        'update_count_callback' => '',
        'query_var' => true,
        'rewrite' => array(
        'slug' => 'flat',
        'with_front' => false,
        'hierarchical' => true,
        'ep_mask' => EP_NONE,
    ),
        'sort' => null,
        '_builtin' => false,
    );
    register_taxonomy('typerealty', 'realty', $args);
}
